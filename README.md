# ExtJS Subdirectory Example

This is a version of a stock Sencha ExtJS application with adjustments to allow
it to coexist with a PHP/Symfony application. The stock build process has been
tweaked to keep most of the code and content in [app/](app/) and generate the
output into `public/app` where it can be exposed using the typical setup for
PHP/Symfony.

The [Makefile](Makefile) automates development tasks: 

* `make build` - Build the production version of the webapp
* `make watch` - Build and _watch_ (i.e. rebuild) the development version of the webapp
* `make run` - Start the PHP webserver to serve up `public/'
* `make clean` - Remove generated and downloaded content

See [this][1] for some notes on the project.

## Issues

* `make watch` requires some links in `public/` that I'd like to avoid if
  possible. I don't know enough about Sencha Cmd to fix this. Specifically, I
  feel I should be able to avoid the `public/main.js`, `public/manifest.json`
  and `public/resources` links that lead to corresponding entries in
  `public/app/`.

## Credit

Paul Dugas <paul@dugas.cc>

Feedback, corrections, suggestions, etc. all welcome.

[1]: https://paul.dugas.cc/post/extjs-subdir/
