build: clean-app node_modules
	npm run build
	@$(RM) -r public/app/temp public/app/archive public/app/deltas

watch: clean-app node_modules
	@mkdir -p public/app
	@ln -s -t public/app ../../app/src ../../app/app.js
	@ln -s -t public ../node_modules
	@ln -s -t public app/main.js app/manifest.json app/resources
	@cp app/index.html public/app
	npm run watch

# Set these on the command-line if necessary
#
# NPM_USER = user..example.com
# NPM_PASS = supersecret
# NPM_USER = user@example.com
#
export NPM_REGISTRY ?= https://npm.sencha.com
export NPM_SCOPE ?= @sencha
define NPM_EXPECT
  spawn npm login --registry=$(NPM_REGISTRY) --scope=$(NPM_SCOPE)
  expect {
    "Username:" {send "$(NPM_USER)\r"; exp_continue}
    "Password:" {send "$(NPM_PASS)\r"; exp_continue}
    "Email: (this IS public)" {send "$(NPM_MAIL)\r"; exp_continue}
  }
endef
export NPM_EXPECT

node_modules: package.json
	@if ! which npm >/dev/null 2>&1; then \
	  echo "error: npm not found in path"; exit 1; \
	fi
	@if ! npm whoami --registry=$(NPM_REGISTRY) >/dev/null 2>&1; then \
	  test -n "$(NPM_USER)" || { echo "error: NPM_USER not set"; exit 1; }; \
	  test -n "$(NPM_PASS)" || { echo "error: NPM_PASS not set"; exit 1; }; \
	  test -n "$(NPM_MAIL)" || { echo "error: NPM_MAIL not set"; exit 1; }; \
	  echo "info: Logging into the $(NPM_REGISTRY) NPM registry..."; \
	  echo "$$NPM_EXPECT" | expect; \
	fi
	npm install --no-progress --no-optional --no-fund --no-audit

run: URL ?= 127.0.0.1:8001
run:
	@[ -d public ] || mkdir public
	php -S $(URL) -t public

clean: clean-app
	@$(RM) -r *.log node_modules
clean-app: 
	@$(RM) -r public/app public/node_modules
	@$(RM) -r public/main.js public/manifest.json public/resources

.PHONY: build watch run clean clean-app
